#!/usr/bin/env python
import sys
import os
import json
import subprocess

etc_hosts_file = "/etc/hosts"
new_etc_hosts_file = "/etc/new_hosts"

def set_etc_hosts_mode(mode, device_name):
    """

    :param mode: There is AutoMode Mode and RemoteMode, AutoMode is when
     the instance is NOT connected to husarnet. RemoteMode when the instance IS
     connected to Husarnet.
    :return:
    """

    # We first remove any elements that shouln't be there
    if mode == "AutoMode":
        bad_words = ['# managed by Husarnet']
    if mode == "RemoteMode":
        bad_words = ['# Added by Theconstruct']

    try:
        with open(etc_hosts_file) as oldfile, open(new_etc_hosts_file, 'w') as newfile:
            for line in oldfile:
                if not any(bad_word in line for bad_word in bad_words):
                    newfile.write(line)

        # We add any element we need for the selected mode
        if mode == "AutoMode":
            new_etc_hosts_file_object = open(new_etc_hosts_file, "a")
            automode_string = "::1 master  # Added by Theconstruct"
            new_etc_hosts_file_object.write(automode_string + "\n")
            automode_string = "::1 "+str(device_name)+"  # Added by Theconstruct"
            new_etc_hosts_file_object.write(automode_string + "\n")

         # Now we remove the old version file and rename the new one
        os.remove(etc_hosts_file)
        os.rename(new_etc_hosts_file, etc_hosts_file)
        return True, 'set etc hosts finished successully'
    except Exception, err:
        return False, err



if __name__ == "__main__":
    
    if len(sys.argv) > 2:
        mode = sys.argv[1]
	device_name = sys.argv[2]        
       	success, msg_out = set_etc_hosts_mode(mode=mode, device_name=device_name)
        print("success="+str(success)+"\nmsg="+str(msg_out))
	if not success:
		print("Remember to execute this script with sudo!")
    else:
        print("success = False\nmsg= Use: sudo python set_etc_hosts_mode.py mode device_name\n(AutoMode For Non Connected, RemoteMode for Connected) ")
