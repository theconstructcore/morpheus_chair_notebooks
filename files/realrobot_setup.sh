#! /usr/bin/env bash
if [ -z "$1" ]
  then
    echo "No Name for Device supplied, Use: realrobot_setup.sh NAME_OF_DEVICE"
    exit 1
fi

echo "Warning_ You should have installed ROS before executing this script... Be warned!"
# Install Husarnet
curl https://install.husarnet.com/install.sh | sudo bash
sudo husarnet-firewall.sh disable

NAME_OF_YOUR_DEVICE=$1

# Add to the end of your Bashrc to use IPV6 and Husarnet
#bash -c "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

bash -c "echo \"export ROS_IPV6=on\" >> ~/.bashrc"
bash -c "echo \"export ROS_HOSTNAME=$NAME_OF_YOUR_DEVICE\" >> ~/.bashrc"
bash -c "echo \"export ROS_MASTER_URI=http://master:11311\" >> ~/.bashrc"
. ~/.bashrc

# Add at the end of your ETC HOSTS
sudo bash -c "echo \"::1 master  # Added by Theconstruct\" >> /etc/hosts"
sudo bash -c "echo \"::1 $NAME_OF_YOUR_DEVICE  # Added by Theconstruct\" >> /etc/hosts"

# Add you rdevice to Husarnet, generate the link to gove to ROSDS
sudo husarnet websetup
 
